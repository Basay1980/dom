<?php
use Carbon\Carbon;

require_once 'vendor/autoload.php';

$carbonNow =  Carbon::now();
$carbonYesterday = Carbon::create(2019, 11, 24);
$carbonMonthAgo = Carbon::create(2019, 10, 25);

echo $carbonNow->format('Y:m:d');
echo '<br>';
echo $carbonYesterday->format('Y:m:d');
echo '<br>';
echo $carbonNow->diffForHumans($carbonMonthAgo);
